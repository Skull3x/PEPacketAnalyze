<?php

/*
 *▪   ▄▄▄·       ▄▄· ▄ •▄ ▄▄▄ .▄▄▄▄▄
 *██ ▐█ ▄█▪     ▐█ ▌▪█▌▄▌▪▀▄.▀·•██
 *▐█· ██▀· ▄█▀▄ ██ ▄▄▐▀▀▄·▐▀▀▪▄ ▐█.▪
 *▐█▌▐█▪·•▐█▌.▐▌▐███▌▐█.█▌▐█▄▄▌ ▐█▌·
 *▀▀▀.▀    ▀█▄▀▪·▀▀▀ ·▀  ▀ ▀▀▀  ▀▀▀
 *
 *This program is free software:
 *and PocketEdition Packet Analyze.
 *
*/

namespace PEPacketAnalyze\protocol\encapsulated;

use PEPacketAnalyze\protocol\Packet;
use PEPacketAnalyze\PEPacketAnalyze;

class UnknownDataPacket extends Packet{

	public function getName(){
		return "UnknownData Packet";
	}

	public function decode(){
		PEPacketAnalyze::getInterface()->getLogger()->debug("UnknownDataPacketが送信されました。 : 0x".bin2hex($this->buffer{0})."", 1);
		file_put_contents(PEPacketAnalyze::getInterface()->getPath()."/0x".bin2hex($this->buffer{0}).".dat", $this->buffer);

		//$this->echo = true;
	}

}
