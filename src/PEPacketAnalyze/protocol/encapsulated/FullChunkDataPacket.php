<?php

/*
 *▪   ▄▄▄·       ▄▄· ▄ •▄ ▄▄▄ .▄▄▄▄▄
 *██ ▐█ ▄█▪     ▐█ ▌▪█▌▄▌▪▀▄.▀·•██
 *▐█· ██▀· ▄█▀▄ ██ ▄▄▐▀▀▄·▐▀▀▪▄ ▐█.▪
 *▐█▌▐█▪·•▐█▌.▐▌▐███▌▐█.█▌▐█▄▄▌ ▐█▌·
 *▀▀▀.▀    ▀█▄▀▪·▀▀▀ ·▀  ▀ ▀▀▀  ▀▀▀
 *
 *This program is free software:
 *and PocketEdition Packet Analyze.
 *
*/

namespace PEPacketAnalyze\protocol\encapsulated;

use PEPacketAnalyze\protocol\Packet;

class FullChunkDataPacket extends Packet{

	public function getName(){
		return "FullChunkData Packet";
	}

	public function decode(){
		$this->chunkX = $this->getInt();
		$this->chunkZ = $this->getInt();
		$this->order = $this->getByte();
		$size = $this->getInt();
		$this->chunkData = $this->get($size);
		//ChunkData Analyze

		//$this->echo = true;
		//print_r($this);
	}

}
