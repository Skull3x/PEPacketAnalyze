<?php

/*
 *▪   ▄▄▄·       ▄▄· ▄ •▄ ▄▄▄ .▄▄▄▄▄
 *██ ▐█ ▄█▪     ▐█ ▌▪█▌▄▌▪▀▄.▀·•██
 *▐█· ██▀· ▄█▀▄ ██ ▄▄▐▀▀▄·▐▀▀▪▄ ▐█.▪
 *▐█▌▐█▪·•▐█▌.▐▌▐███▌▐█.█▌▐█▄▄▌ ▐█▌·
 *▀▀▀.▀    ▀█▄▀▪·▀▀▀ ·▀  ▀ ▀▀▀  ▀▀▀
 *
 *This program is free software:
 *and PocketEdition Packet Analyze.
 *
*/

namespace PEPacketAnalyze\protocol\encapsulated;

use PEPacketAnalyze\protocol\Packet;

class UpdateBlockPacket extends Packet{

	public function getName(){
		return "UpdateBlock Packet";
	}

	public function decode(){
		$this->records = [];
		$count = $this->getInt();
		for($i = 0; $i < $count; $i++){
			$x = $this->getInt();
			$z = $this->getInt();
			$y = $this->getByte();
			$blockid = $this->getByte();
			$blockdata = $this->getByte();
			$this->records[] = ["x" => $x, "z" => $z, "y" => $y, "id" => $blockid, "data" => $blockdata >> 60];
		}
		
		$this->echo = true;
		//print_r($this);
	}

}
